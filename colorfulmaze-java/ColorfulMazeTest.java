import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ColorfulMazeTest {

    protected ColorfulMaze solution;

    @Before
    public void setUp() {
        solution = new ColorfulMaze();
    }

    public static void assertEquals(double expected, double actual) {
        if (Double.isNaN(expected)) {
            Assert.assertTrue("expected: <NaN> but was: <" + actual + ">", Double.isNaN(actual));
            return;
        }
        double delta = Math.max(1e-9, 1e-9 * Math.abs(expected));
        Assert.assertEquals(expected, actual, delta);
    }

    @Test
    public void testCase0() {
        String[] maze = new String[]{".$.", "A#B", "A#B", ".!."};
        int[] trap = new int[]{50, 40, 0, 0, 0, 0, 0};

        double expected = 0.8;
        double actual = solution.getProbability(maze, trap);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase1() {
        String[] maze = new String[]{".$.", "A#B", "A#C", ".!."};
        int[] trap = new int[]{20, 70, 40, 0, 0, 0, 0};

        double expected = 0.86;
        double actual = solution.getProbability(maze, trap);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase2() {
        String[] maze = new String[]{"$A#", ".#.", "#B!"};
        int[] trap = new int[]{10, 10, 10, 10, 10, 10, 10};

        double expected = 0.0;
        double actual = solution.getProbability(maze, trap);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase3() {
        String[] maze = new String[]{"$A..", "##.A", "..B!"};
        int[] trap = new int[]{50, 50, 0, 0, 0, 0, 0};

        double expected = 0.75;
        double actual = solution.getProbability(maze, trap);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase4() {
        String[] maze = new String[]{"$C..", "##.A", "..B!"};
        int[] trap = new int[]{50, 50, 100, 0, 0, 0, 0};

        double expected = 0.5;
        double actual = solution.getProbability(maze, trap);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase5() {
        String[] maze = new String[]{".$.D.E.F.G.!."};
        int[] trap = new int[]{10, 20, 30, 40, 50, 60, 70};

        double expected = 0.23400000000000004;
        double actual = solution.getProbability(maze, trap);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase6() {
        String[] maze = new String[]{"CC...AA", "C##.##A", "!.E.E.$", "D##.##B", "DD...BB"};
        int[] trap = new int[]{90, 90, 25, 50, 75, 0, 0};

        double expected = 0.8125;
        double actual = solution.getProbability(maze, trap);

        assertEquals(expected, actual);
    }

}
