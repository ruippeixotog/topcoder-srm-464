import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ColorfulStringsTest {

    protected ColorfulStrings solution;

    @Before
    public void setUp() {
        solution = new ColorfulStrings();
    }

    @Test
    public void testCase0() {
        int n = 3;
        int k = 4;

        String expected = "238";
        String actual = solution.getKth(n, k);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase1() {
        int n = 4;
        int k = 2000;

        String expected = "";
        String actual = solution.getKth(n, k);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase2() {
        int n = 5;
        int k = 1;

        String expected = "23457";
        String actual = solution.getKth(n, k);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase3() {
        int n = 2;
        int k = 22;

        String expected = "52";
        String actual = solution.getKth(n, k);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase4() {
        int n = 6;
        int k = 464;

        String expected = "257936";
        String actual = solution.getKth(n, k);

        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void testCase5() {
        int n = 1;
        int k = 1;

        String expected = "0";
        String actual = solution.getKth(n, k);

        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void testCase6() {
        int n = 1;
        int k = 3;

        String expected = "2";
        String actual = solution.getKth(n, k);

        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void testCase10() {
        int n = 1;
        int k = 8;

        String expected = "7";
        String actual = solution.getKth(n, k);

        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void customTestCase0() {
        int n = 50;
        int k = 1000000000;

        String expected = "";
        String actual = solution.getKth(n, k);

        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void customTestCase1() {
        int n = 8;
        int k = 1000000000;

        String expected = "";
        String actual = solution.getKth(n, k);

        Assert.assertEquals(expected, actual);
    }
}
