import java.util.HashSet;
import java.util.Set;

public class ColorfulStrings {

	public String getKth(int n, int k) {
		if (n > 8) return "";
		if (n == 1) return k > 10 ? "" : "" + (k - 1);
		kLeft = k;
		return gen(new boolean[8], new int[n], 0);
	}
	
	int kLeft;

	String gen(boolean[] isUsed, int[] current, int currN) {
		if(currN == current.length) {
			if(isColorful(current)) {
				kLeft--;
				return kLeft == 0 ? genString(current) : "";
			}
			return "";
		}
		for(int i = 0; i < isUsed.length; i++) {
			if(!isUsed[i]) {
				current[currN] = i + 2;
				isUsed[i] = true;
				String res = gen(isUsed, current, currN + 1);
				if(res != "") return res;
				isUsed[i] = false;
			}
		}
		return "";
	}
	
	boolean isColorful(int[] curr) {
		Set<Integer> prodHash = new HashSet<Integer>();
		for(int i = 0; i < curr.length; i++) {
			int prod = 1;
			for(int j = i; j < curr.length; j++) {
				prod *= curr[j];
				if(prodHash.contains(prod)) return false;
				prodHash.add(prod);
			}
		}
		return true;
	}

	String genString(int[] curr) {
		String str = "";
		for(int i: curr) {
			str += i;
		}
		return str;
	}
}
