import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ColorfulDecorationTest {

    protected ColorfulDecoration solution;

    @Before
    public void setUp() {
        solution = new ColorfulDecoration();
    }

    @Test
    public void testCase0() {
        int[] xa = new int[]{10, 0, 7};
        int[] ya = new int[]{0, 19, 6};
        int[] xb = new int[]{20, 10, 25};
        int[] yb = new int[]{20, 35, 25};

        int expected = 19;
        int actual = solution.getMaximum(xa, ya, xb, yb);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase1() {
        int[] xa = new int[]{464, 20};
        int[] ya = new int[]{464, 10};
        int[] xb = new int[]{464, 3};
        int[] yb = new int[]{464, 16};

        int expected = 461;
        int actual = solution.getMaximum(xa, ya, xb, yb);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase2() {
        int[] xa = new int[]{0, 0, 1, 1};
        int[] ya = new int[]{0, 0, 1, 1};
        int[] xb = new int[]{1, 1, 0, 0};
        int[] yb = new int[]{1, 1, 0, 0};

        int expected = 0;
        int actual = solution.getMaximum(xa, ya, xb, yb);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase3() {
        int[] xa = new int[]{0, 3, 0, 5, 6};
        int[] ya = new int[]{1, 6, 0, 8, 5};
        int[] xb = new int[]{6, 1, 7, 4, 7};
        int[] yb = new int[]{5, 9, 2, 8, 9};

        int expected = 3;
        int actual = solution.getMaximum(xa, ya, xb, yb);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase4() {
        int[] xa = new int[]{1000000000, 0};
        int[] ya = new int[]{0, 1000000000};
        int[] xb = new int[]{0, 1000000000};
        int[] yb = new int[]{0, 1000000000};

        int expected = 1000000000;
        int actual = solution.getMaximum(xa, ya, xb, yb);

        Assert.assertEquals(expected, actual);
    }

}
